import { gql } from "apollo-boost";

export const GET_SEARCH = gql`
  query GET_SEARCH($q: String, $per_page: Int, $sort: String) {
    search(q: $q, per_page: $per_page, sort: $sort) {
      products {
        id
        title
        images {
          still_s
          over_s
        }
        brand
        pricing {
          app_effective_price
          base_price
          discount
          effective_price
        }
      }
      info {
        title
      }
      sorts {
        label
        value
        is_selected
      }
    }
  }
`;

// use css column for something like pinterest style
import { IArg } from "../types";

export const ensureInt = (val: any, def?: number) => {
  return (val && parseInt(val, 0)) || def || 0;
};

export function defaultQuery(
  args: IArg = {
    per_page: 36,
    q: "baju",
    sort: "energy desc"
  }
) {
  const { q, per_page, sort } = args;

  return {
    per_page: ensureInt(per_page, 36),
    q,
    sort
  };
}

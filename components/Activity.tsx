import * as React from "react";

interface Iactivity {
  isLoading: boolean;
  isEmpty: boolean;
  responseView: React.ReactElement;
}

const Activity: React.SFC<Iactivity> = (props: Iactivity) => {
  const { isEmpty, isLoading, responseView } = props;

  if (isLoading) {
    return <div>loading...</div>;
  }

  if (isEmpty) {
    return <div>empty</div>;
  } else {
    return <React.Fragment>{responseView}</React.Fragment>;
  }
};

export default Activity;

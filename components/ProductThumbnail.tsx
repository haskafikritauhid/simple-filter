import Link from "next/link";
import * as React from "react";
import { Iproduct } from "../types";

interface IproductThumbnail {
  item: Iproduct;
}

const ProductThumbnail: React.SFC<IproductThumbnail> = (
  props: IproductThumbnail
) => {
  const { item } = props;

  const [imageSource, setImageSource] = React.useState(item.images.still_s);

  return (
    <article key={item.id} className="thumbnail">
      <div className="thumbnail__image">
        <Link href="">
          <img
            src={imageSource}
            alt=""
            onMouseOver={() => setImageSource(item.images.over_s)}
            onMouseOut={() => setImageSource(item.images.still_s)}
          />
        </Link>
      </div>
      <div className="thumbnail__info">
        <p>
          <span>{item.brand}</span>
        </p>

        <p className="thumbnail__title">
          <span>{item.title}</span>
        </p>
        <p>
          <span>{item.pricing.app_effective_price}</span>
        </p>
        <p className="thumbnail__old-price">
          <span>{item.pricing.base_price}</span>
        </p>
      </div>
      <style jsx>{`
        .thumbnail {
          flex: 0 0 auto;
          white-space: normal;
          border: none;
          box-sizing: border-box;
          position: relative;
          width: 33.333%;
          margin-bottom: 2em;
          padding-left: 8px;
          padding-right: 8px;
        }
        .thumbnail__info {
          max-width: 312px;
          margin: 0 auto;
        }
        .thumbnail__image {
          display: flex;
          justify-content: center;
        }
        .thumbnail__title {
          max-width: 312px;
          white-space: nowrap;
          text-overflow: ellipsis;
          overflow: hidden;
          cursor: pointer;
        }
        .thumbnail__old-price {
          text-decoration: line-through;
        }
      `}</style>
    </article>
  );
};

export default ProductThumbnail;

import Head from "next/head";
import * as React from "react";

interface IlayoutProps {
  children: React.ReactNode;
}

const Layout: React.SFC<IlayoutProps> = (props: IlayoutProps) => {
  const { children } = props;
  return (
    <React.Fragment>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css"
        />
        <meta charSet="utf-8" />
      </Head>
      <main className="main">{children}</main>

      <style jsx global>{`
        * {
          box-sizing: border-box;
        }
        body {
          font-family: interface, "Helvetica Neue", helvetica, sans-serif;
          font-size: 12px;
        }
        p {
          line-height: normal;
        }
        input[type="text"] {
          padding: 4px 10px;
        }
        .spacer {
          margin: 10px;
        }
        .main {
          margin: 0 auto;
          width: calc(100% - 100px);
          max-width: 1440px;
          padding: 0;
          overflow: visible;
        }
      `}</style>
    </React.Fragment>
  );
};

export default Layout;

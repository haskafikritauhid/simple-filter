import * as React from "react";
import { ProductThumbnail } from "../components";
import { Products as ProductsProps } from "../types";

interface IproductsProps {
  data: ProductsProps;
}

const Products: React.SFC<IproductsProps> = (props: IproductsProps) => {
  const { data } = props;
  return (
    <div className="products">
      {data.map(item => (
        <ProductThumbnail item={item} key={item.id} />
      ))}
      <style jsx>{`
        .products {
          display: flex;
          flex-wrap: wrap;
          flex-wrap: wrap;
          overflow: hidden;
        }
      `}</style>
    </div>
  );
};

export default Products;

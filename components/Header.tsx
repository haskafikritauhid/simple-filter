import * as React from "react";
import { Search } from ".";
import { Ioption } from "../types";

interface IheaderProps {
  onSearch: (val: string) => void;
  onFilter?: (val: string) => void;
  sorts?: Ioption[];
}

const Header: React.SFC<IheaderProps> = (props: IheaderProps) => {
  const { onFilter, onSearch, sorts } = props;
  const [filter, setFilter] = React.useState("");

  const changeFilter = (val: string) => {
    setFilter(val);
    return val && val.length && onFilter && onFilter(val);
  };

  const changeSearch = (e: React.SyntheticEvent, val: string) => {
    e.preventDefault();
    setFilter("");
    window.scrollTo(0, 0);
    return val && val.length && onSearch(val);
  };

  return (
    <div className="header">
      <Search onSearch={changeSearch} />
      <span className="spacer" />
      {sorts && (
        <select onChange={e => changeFilter(e.target.value)} value={filter}>
          <option value="">Pilih Filter</option>
          {sorts.map(sort => (
            <option key={sort.value} value={sort.value}>
              {sort.label}
            </option>
          ))}
        </select>
      )}
      <style jsx>{`
        .header {
          display: inline-flex;
          position: sticky;
          top: 0;
          z-index: 1;
          padding: 1em;
        }
      `}</style>
    </div>
  );
};

export default Header;

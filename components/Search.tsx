import * as React from "react";

interface IsearchProps {
  onSearch: (e: React.SyntheticEvent<HTMLFormElement>, val: string) => void;
}

const Search: React.SFC<IsearchProps> = (props: IsearchProps) => {
  const { onSearch } = props;
  const [searchText, setSearchText] = React.useState("");

  return (
    <div className="searchbar">
      <form onSubmit={e => onSearch(e, searchText)} id="search">
        <input
          name="search"
          type="text"
          onChange={e => setSearchText(e.target.value)}
          autoFocus
          placeholder="Search..."
          className="search-input"
          value={searchText}
        />
      </form>
    </div>
  );
};

export default Search;

import * as React from "react";

interface IproductsTitle {
  name: string;
}

const ProductsTitle: React.SFC<IproductsTitle> = (props: IproductsTitle) => {
  const { name } = props;
  return (
    <div className="page-title">
      <h3 className="page-title__text">{name}</h3>
      <style jsx>{`
        .page-title {
          text-align: center;
        }
        .page-title__text {
          text-transform: uppercase;
        }
      `}</style>
    </div>
  );
};

export default ProductsTitle;

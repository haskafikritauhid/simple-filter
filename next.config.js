const nextRuntimeDotenv = require("next-runtime-dotenv");

const withConfig = nextRuntimeDotenv({
  // path: '.env',
  public: ["ENV_SERVER_URL"]
});

module.exports = withConfig({
  // Your Next.js config.
});

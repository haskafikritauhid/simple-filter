// Stateless helper type since next not provide this
export interface IstatelessPage<P = {}> extends React.SFC<P> {
  getInitialProps?: (ctx: any) => Promise<P>;
}

export interface Ioption {
  label: string;
  value: string;
}

export interface IArg {
  q: string;
  sort?: string;
  per_page?: number;
}

export interface Ipricing {
  app_effective_price: string;
  base_price: string;
  discount: string;
  effective_price: string;
}

export interface Images {
  still_s: string;
  over_s: string;
}

export interface Iproduct {
  id: number;
  title: string;
  brand: string;
  images: Images;
  pricing: Ipricing;
}

export type Products = Iproduct[];

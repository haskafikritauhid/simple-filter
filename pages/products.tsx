import { useQuery } from "@apollo/react-hooks";
import get from "lodash.get";
import Router from "next/router";
import qs from "query-string";
import * as React from "react";
import {
  Activity,
  Header,
  Layout,
  Products,
  ProductsTitle
} from "../components";
import withData from "../lib/apollo";
import { GET_SEARCH } from "../queries";
import { IstatelessPage } from "../types";
import { defaultQuery } from "../utils";

interface IsearchPage {
  q: any;
}

const HomePage: IstatelessPage<IsearchPage> = (props: IsearchPage) => {
  const { q } = props;
  const { data, loading } = useQuery(GET_SEARCH, {
    notifyOnNetworkStatusChange: true,
    variables: defaultQuery({ ...q })
  });
  const search = (query: string) => Router.replace(`/products?q=${query}`);
  const filter = (val: string) => {
    const queries = qs.stringify({ ...q, sort: val });
    Router.replace(`/products?${queries}`);
  };
  const products = get(data, "search.products", []);

  return (
    <Layout>
      <Header
        onSearch={val => search(val)}
        sorts={get(data, "search.sorts", [])}
        onFilter={val => filter(val)}
      />
      <ProductsTitle name={get(data, "search.info.title", "...")} />
      <Activity
        isLoading={loading}
        isEmpty={products.length === 0}
        responseView={<Products data={products} />}
      />
    </Layout>
  );
};

HomePage.getInitialProps = async ({ query }) => {
  return { q: query };
};

export default withData(HomePage);

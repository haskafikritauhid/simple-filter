import Router from "next/router";
import * as React from "react";
import { Header, Layout } from "../components";
import { IstatelessPage } from "../types";

const HomePage: IstatelessPage<{}> = () => {
  const search = (query: string) => Router.replace(`/products?q=${query}`);

  return (
    <Layout>
      <Header onSearch={val => search(val)} />
      <div className="logo">
        <img src="/static/mds-logo.svg/" width={300} />
      </div>
      <style jsx>{`
        .logo {
          position: fixed;
          top: 50%;
          left: 50%;
          transform: translateX(-50%) translateY(-50%);
        }
      `}</style>
    </Layout>
  );
};

export default HomePage;

import { ensureInt } from "../utils";

describe("utils", () => {
  it("ensure to integer", () => {
    expect(ensureInt("2")).toBe(2);
    expect(ensureInt("")).toBe(0);
    expect(ensureInt("s", 3)).toBe(3);
    expect(ensureInt(3)).toBe(3);
  });
});

export // Use an empty export to please Babel's single file emit.
// https://github.com/Microsoft/TypeScript/issues/15230
{};

import { HttpLink } from "apollo-boost";
// @ts-ignore
import { withData } from "next-apollo";
import getConfig from "next/config";

const {
  publicRuntimeConfig: { ENV_SERVER_URL } // Available both client and server side
} = getConfig();

const config = {
  link: new HttpLink({
    uri: ENV_SERVER_URL // Server URL (must be absolute)
  })
};

export default withData(config);

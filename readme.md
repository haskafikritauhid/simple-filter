# simple submission
A very basic example 

# Requirement
- [x] search input
- [x] search integration basic
- [x] dropdown filter
- [x] dropdown filter dari api
- [x] loading state
- [x] menampilkan produk at least 36
- [x] empty state
- [x] thumbnail onhover
- [x] nama brand
- [x] nama produk
- [x] harga efektif
- [x] harga dasar dicoret kalau ada diskon

## How to run

```
cd client && npm i && npm run build && npm run start
```

## Stack
- Apollo (Graphql)
- Next Js (SSR)
- React 
